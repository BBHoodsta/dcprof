# dcprof

dcprof is a gprof compatible sampling profiler for the Sega Dreamcast + KallistiOS.

## Usage

First include profiler.h and profiler.c in your project.

In your application, at the start of main usually, you put the following code:

```
    profiler_init("/pc/gmon.out");
    profiler_start();
```

Then, at the end of your program (possibly using `std::set_terminate` or `atexit`) you do this:

```
    profiler_stop();
    profiler_clean_up();
```

`profiler_init` starts a background thread for taking samples, but it doesn't actually do anything until you call 
`profiler_start` (this is so you can choose where to start/stop your profiling).

The file path passed in is where the profiling data will be written to. You will only get a file generated
if you are running your application from within the `dcload` tool which mounts a writable `/pc` directory.

If the directory isn't writable, you should get CSV output written to the terminal but you'll likely
need to post-process that to something useful.

## How it Works

Once profiling has begun, the background thread will regularly gather the PC and PR registers stored by
the other threads.

The way thread scheduling works is that when other threads are blocked their current program counter is stored
in a context. If the profiler thread is doing work then all the other threads aren't and so the stored program
counters will be up-to-date.

The profiling thread gathers PC/PR pairs and how often that pairing appears.

## Limitations

 - Currently only the main thread is recorded, although with more testing that will change.
 - The sample rate is always going to be approximate because it's dependent on thread scheduling, it would
 be better to use a time interuppt but that might break threading and it gets complicated quickly.
 
## License

dcprof is released under the MIT license.
